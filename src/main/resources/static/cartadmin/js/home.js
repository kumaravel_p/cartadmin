var SkavaCart = Class.extend(
{
    init: function() {
        this.reader();
    },
    reader: function() {
        var _this = this;

        _this.registerEvent();
    },
    registerEvent: function() {
        var _this = this;

        $(".update-config-btn").click(function () {
            $('#sampleForm').submit(function(event){

            });
        });

        $(".save-cart-config").click(function () {
            $("#exampleModal").modal("toggle");
            $('.modal-backdrop').remove();
            toastr["success"]("Cart configs are updated successfully");
        });
    }
});

new SkavaCart();
