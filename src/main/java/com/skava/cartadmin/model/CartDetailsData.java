package com.skava.cartadmin.model;

public class CartDetailsData
{

    private Integer userMaxCartQuantity;
    private Integer maxCartQuantity;
    private Integer minCartValue;
    private Integer maxCartValue;
    private String shippingMethod;

    public CartDetailsData(Integer userMaxCartQuantity,
                           Integer maxCartQuantity,
                           Integer minCartValue,
                           Integer maxCartValue,
                           String shippingMethod)
    {
        this.userMaxCartQuantity = userMaxCartQuantity;
        this.maxCartQuantity = maxCartQuantity;
        this.minCartValue = minCartValue;
        this.maxCartValue = maxCartValue;
        this.shippingMethod = shippingMethod;
    }

    public Integer getUserMaxCartQuantity()
    {
        return userMaxCartQuantity;
    }

    public void setUserMaxCartQuantity(Integer userMaxCartQuantity)
    {
        this.userMaxCartQuantity = userMaxCartQuantity;
    }

    public Integer getMaxCartQuantity()
    {
        return maxCartQuantity;
    }

    public void setMaxCartQuantity(Integer maxCartQuantity)
    {
        this.maxCartQuantity = maxCartQuantity;
    }

    public Integer getMinCartValue()
    {
        return minCartValue;
    }

    public void setMinCartValue(Integer minCartValue)
    {
        this.minCartValue = minCartValue;
    }

    public Integer getMaxCartValue()
    {
        return maxCartValue;
    }

    public void setMaxCartValue(Integer maxCartValue)
    {
        this.maxCartValue = maxCartValue;
    }

    public String getShippingMethod()
    {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod)
    {
        this.shippingMethod = shippingMethod;
    }

}
