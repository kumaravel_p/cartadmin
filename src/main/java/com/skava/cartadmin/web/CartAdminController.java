package com.skava.cartadmin.web;

import com.skava.cartadmin.model.CartDetailsData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller public class CartAdminController
{
    @RequestMapping("/ping") public @ResponseBody String ping()
    {
        return "CartAdminController: responding to a ping - " + System.currentTimeMillis();
    }

    @RequestMapping({ "/home", "/" }) public ModelAndView homePage()
    {
        CartDetailsData cdd = new CartDetailsData(30, 50, 0, 500, "express");

        ModelAndView model = new ModelAndView();
        model.setViewName("layout/content-hf");
        model.addObject("pageName", "home");
        model.addObject("cart", cdd);
        model.addObject("contentTemplate", "page/common/home");

        return model;
    }
}
